﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using AirWaysClient.Models;

namespace AirWaysClient.Controllers
{
    public class LoginController : Controller
    {
        /// <summary>
        /// Cliente Http para hacer los request a la Api.
        /// Este objeto lo creo de forma estatica para poder reutilizarlo en toda la aplicacion sin tener que
        /// instanciarlo nuevamente. asi se evitan errores de conexiones a la api.
        /// </summary>
        private static HttpClient client = HttpClientTaker.GetClient();

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string password, string username)
        {
            LoginRequest request = new LoginRequest
            {
                Password = password,
                Username = username
            };
            var resultado = client.PostAsJsonAsync("api/login/authenticate", request).Result;
            if (resultado.IsSuccessStatusCode)
            {
                string token = resultado.Content.ReadAsAsync<string>().Result;
                ViewBag.mensajePositivo = "Loguin Exitoso";
                SetCookie(token);
            }
            else
            {
                ViewBag.mensajeNegativo = resultado.ReasonPhrase;
            }
            return View("Index");
        }

        private void SetCookie(string valor)
        {
            var cookieCLient = HttpContext.Request.Cookies.Get("tkn");
            if (cookieCLient == null)
            {
                cookieCLient = new HttpCookie("tkn")
                {
                    Value = valor,
                    Expires = DateTime.Now.AddMinutes(30),
                };
                HttpContext.Response.Cookies.Add(cookieCLient);
            }
            else
            {
                cookieCLient.Value = valor;
                cookieCLient.Expires = DateTime.Now.AddMinutes(30);
                HttpContext.Response.Cookies.Set(cookieCLient);
            }
        }
    }
}