﻿using AirWaysClient.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace AirWaysClient.Controllers
{
    public class CustomersController : Controller
    {
        /// <summary>
        /// Cliente Http para hacer los request a la Api.
        /// Este objeto lo creo de forma estatica para poder reutilizarlo en toda la aplicacion sin tener que
        /// instanciarlo nuevamente. asi se evitan errores de conexiones a la api.
        /// </summary>
        private static HttpClient client = HttpClientTaker.GetClient();

        // GET: Customers
        public ActionResult Index(string mensajePositivo = null, string mensajeNegativo = null)
        {
            //Setear las propiedades de los mensajes que se van a mostrar en el fromnt.
            if (mensajePositivo != null)
            {
                ViewBag.mensajePositivo = mensajePositivo;
            }
            if (mensajeNegativo != null)
            {
                ViewBag.mensajeNegativo = mensajeNegativo;
            }

            List<Customer> customers = new List<Customer>();
            client = HttpClientTaker.GetTokenFromCookieAndSetToHttpClient(client);
            var resultado = client.GetAsync("api/customers").Result;
            if (resultado.IsSuccessStatusCode)
            {
                customers = resultado.Content.ReadAsAsync<List<Customer>>().Result;
            }
            else
            {
                ViewBag.mensajeNegativo = resultado.ReasonPhrase;
            }
            return View(customers);
        }

        [HttpGet]
        public ActionResult Create()
        {
            Customer c = new Customer();
            return View(c);
        }

        [HttpPost]
        public ActionResult Create(Customer model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.mensajeNegativo = "El modelo no es valido, Revise los datos";
                return View(model);
            }

            if (string.IsNullOrEmpty(model.CustomerAdress) || string.IsNullOrEmpty(model.CustomerIDCard) || string.IsNullOrEmpty(model.CustomerName) || string.IsNullOrEmpty(model.CustomerPhone))
            {
                ViewBag.mensajeNegativo = "Debe llenar todos los campos del formulario";
                return View(model);
            }

            var resultado = client.PostAsJsonAsync("api/customers", model).Result;
            if (resultado.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", new { mensajePositivo = "Informacion guardada de forma Exitosa" });
            }
            else
            {
                ViewBag.mensajeNegativo = resultado.ReasonPhrase;
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var resultado = client.GetAsync("api/customers/" + id).Result;
            if (resultado.IsSuccessStatusCode)
            {
                var cutomer = resultado.Content.ReadAsAsync<Customer>().Result;
                return View(cutomer);
            }
            else
            {
                return RedirectToAction("Index", new { mensajeNegativo = resultado.ReasonPhrase });
            }
        }

        /// <summary>
        /// Metodo para hacer Update de un customer.
        /// el metodo redirige al index con una mensaje positivo o negativo dependiendo del resultado de la operacion en la api
        /// </summary>
        /// <param name="customer">el customer que se desea modificar</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.mensajeNegativo = "El modelo de datos que introdjo no es valido";
                return View(customer);
            }
            var resultado = client.PutAsJsonAsync("api/customers/" + customer.CustomerID, customer).Result;
            if (resultado.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", new { mensajePositivo = "Informacion guardada de manera exitosa" });
            }
            else
            {
                return RedirectToAction("Index", new { mensajeNegativo = resultado.ReasonPhrase });
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var resultado = client.GetAsync("api/customers/" + id).Result;
            if (resultado.IsSuccessStatusCode)
            {
                var destino = resultado.Content.ReadAsAsync<Customer>().Result;
                return View(destino);
            }
            else
            {
                return RedirectToAction("Index", new { mensajeNegativo = resultado.ReasonPhrase });
            }
        }

        [HttpPost]
        public ActionResult Delete(Customer customer)
        {
            var resultado = client.DeleteAsync("api/customers/" + customer.CustomerID).Result;
            if (resultado.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", new { mensajePositivo = "Informacion guardada de manera exitosa" });
            }
            else
            {
                return RedirectToAction("Index", new { mensajeNegativo = resultado.ReasonPhrase });
            }
        }
    }
}