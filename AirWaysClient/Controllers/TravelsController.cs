﻿using AirWaysClient.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace AirWaysClient.Controllers
{
    public class TravelsController : Controller
    {
        /// <summary>
        /// Cliente Http para hacer los request a la Api.
        /// Este objeto lo creo de forma estatica para poder reutilizarlo en toda la aplicacion sin tener que
        /// instanciarlo nuevamente. asi se evitan errores de conexiones a la api.
        /// </summary>
        private static HttpClient client = HttpClientTaker.GetClient();

        public static List<Destination> destinations = new List<Destination>();
        // GET: Customers
        public ActionResult Index(string mensajePositivo = null, string mensajeNegativo = null)
        {
            //Setear las propiedades de los mensajes que se van a mostrar en el fromnt.
            if (mensajePositivo != null)
            {
                ViewBag.mensajePositivo = mensajePositivo;
            }
            if (mensajeNegativo != null)
            {
                ViewBag.mensajeNegativo = mensajeNegativo;
            }

            List<TravelViewModel> travels = new List<TravelViewModel>();
            var resultado = client.GetAsync("api/travels").Result;
            if (resultado.IsSuccessStatusCode)
            {
                travels = resultado.Content.ReadAsAsync<List<TravelViewModel>>().Result;
            }
            else
            {
                ViewBag.mensajeNegativo = resultado.ReasonPhrase;
            }
            return View(travels);
        }

        [HttpGet]
        public ActionResult Create()
        {
            destinations = client.GetAsync("api/destinations").Result.Content.ReadAsAsync<List<Destination>>().Result;
            destinations.Add(new Destination
            {
                DestinationID = 0,
                DestinationCode = "Sleccione.."
            });
            ViewBag.destinos = new SelectList(destinations, "DestinationID", "DestinationCode");
            Travel t = new Travel();
            return View(t);
        }

        [HttpPost]
        public ActionResult Create(Travel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.mensajeNegativo = "El modelo no es valido, Revise los datos";
                ViewBag.destinos = new SelectList(destinations, "DestinationID", "DestinationCode");
                return View(model);
            }

            if (!ValidateTravelData(model))
            {
                ViewBag.mensajeNegativo = "Por favor valide que los datos ingresados cumplan con la politica de registro de vuelos vuelos.";
                ViewBag.destinos = new SelectList(destinations, "DestinationID", "DestinationCode");
                return View(model);
            }

            //model.TravelDate = model.TravelDate.AddHours(int.Parse(model.TravelHour.Substring(0, 2))).AddMinutes(int.Parse(model.TravelHour.Substring(3, 2)));
            model.TravelAvailableSeats = model.TravelSeatCapacity;
            var resultado = client.PostAsJsonAsync("api/travels", model).Result;
            if (resultado.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", new { mensajePositivo = "Informacion guardada de forma Exitosa" });
            }
            else
            {
                ViewBag.mensajeNegativo = resultado.ReasonPhrase;
                return View(model);
            }
        }

        /// <summary>
        /// Validar la informacion relacionada a un vuelo. 
        /// se valida que el destino y el origen no sean los mismos
        /// que se registre con al menos un puesto disponible
        /// que tenga precio
        /// </summary>
        /// <param name="t">objeto Travel a valiar</param>
        /// <returns>verdadero si los datos son valido, falso si los datos son no validos.</returns>
        private bool ValidateTravelData(Travel t)
        {
            bool valido = true;
            if (t.TravelSeatCapacity <= 0)
            {
                return !valido;
            }

            if (t.TravelSource == t.TravelTarget)
            {
                return !valido;
            }

            if (t.TravelPrice <= 0)
            {
                return !valido;
            }

            t.TravelDate = t.TravelDate.AddHours(int.Parse(t.TravelHour.Substring(0, 2))).AddMinutes(int.Parse(t.TravelHour.Substring(3,2)));

            if (t.TravelDate <= DateTime.Now)
            {
                return !valido;
            }
            return valido;
        }

        [HttpGet]
        public ActionResult Update(int id)
        {
            destinations = client.GetAsync("api/destinations").Result.Content.ReadAsAsync<List<Destination>>().Result;

            var resultado = client.GetAsync("api/travels/" + id).Result;
            if (resultado.IsSuccessStatusCode)
            {
                var travel = resultado.Content.ReadAsAsync<Travel>().Result;
                travel.TravelHour = travel.TravelDate.ToString("HH:mm");
                ViewBag.traveldate = travel.TravelDate.ToString("yyyy-MM-dd");
                ViewBag.destinos = new SelectList(destinations, "DestinationID", "DestinationCode");
                return View(travel);
            }
            else
            {
                return RedirectToAction("Index", new { mensajeNegativo = resultado.ReasonPhrase });
            }
        }

        /// <summary>
        /// Metodo para hacer Update de un trvel.
        /// el metodo redirige al index con una mensaje positivo o negativo dependiendo del resultado de la operacion en la api
        /// </summary>
        /// <param name="travel">el travel que se desea modificar</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(Travel travel)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.destinos = new SelectList(destinations, "DestinationID", "DestinationCode");
                ViewBag.mensajeNegativo = "El modelo de datos que introdjo no es valido";
                return View(travel);
            }

            if (!ValidateTravelData(travel))
            {
                ViewBag.mensajeNegativo = "Por favor valide que los datos ingresados cumplan con la politica de registro de vuelos vuelos.";
                ViewBag.destinos = new SelectList(destinations, "DestinationID", "DestinationCode");
                return View(travel);
            }

            //travel.TravelDate = travel.TravelDate.AddHours(int.Parse(travel.TravelHour.Substring(0, 2))).AddMinutes(int.Parse(travel.TravelHour.Substring(3, 2)));

            var resultado = client.PutAsJsonAsync("api/travels/" + travel.TravelID, travel).Result;
            if (resultado.IsSuccessStatusCode)
            {
                //var destino = resultado.Content.ReadAsAsync<Destination>().Result;
                return RedirectToAction("Index", new { mensajePositivo = "Informacion guardada de manera exitosa" });
            }
            else
            {
                return RedirectToAction("Index", new { mensajeNegativo = resultado.ReasonPhrase });
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var resultado = client.GetAsync("api/travels/" + id).Result;
            if (resultado.IsSuccessStatusCode)
            {
                var travel = resultado.Content.ReadAsAsync<TravelViewModel>().Result;
                return View(travel);
            }
            else
            {
                return RedirectToAction("Index", new { mensajeNegativo = resultado.ReasonPhrase });
            }
        }

        [HttpPost]
        public ActionResult Delete(Travel travel)
        {
            var resultado = client.DeleteAsync("api/travels/" + travel.TravelID).Result;
            if (resultado.IsSuccessStatusCode)
            {
                //var destino = resultado.Content.ReadAsAsync<Destination>().Result;
                return RedirectToAction("Index", new { mensajePositivo = "Informacion guardada de manera exitosa" });
            }
            else
            {
                return RedirectToAction("Index", new { mensajeNegativo = resultado.ReasonPhrase });
            }
        }
    }
}