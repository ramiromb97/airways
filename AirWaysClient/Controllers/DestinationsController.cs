﻿using AirWaysClient.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace AirWaysClient.Controllers
{
    public class DestinationsController : Controller
    {
        /// <summary>
        /// Cliente Http para hacer los request a la Api.
        /// Este objeto lo creo de forma estatica para poder reutilizarlo en toda la aplicacion sin tener que
        /// instanciarlo nuevamente. asi se evitan errores de conexiones a la api.
        /// </summary>
        private static HttpClient client = HttpClientTaker.GetClient();

        // GET: Destinations
        public ActionResult Index(string mensajePositivo = null, string mensajeNegativo = null)
        {
            //Setear las propiedades de los mensajes que se van a mostrar en el fromnt.
            if (mensajePositivo != null)
            {
                ViewBag.mensajePositivo = mensajePositivo;
            }
            if (mensajeNegativo != null)
            {
                ViewBag.mensajeNegativo = mensajeNegativo;
            }

            List<Destination> destinos = new List<Destination>();
            var resultado = client.GetAsync("api/destinations").Result;
            if (resultado.IsSuccessStatusCode)
            {
                destinos = resultado.Content.ReadAsAsync<List<Destination>>().Result;
            }
            else
            {
                ViewBag.mensajeNegativo = resultado.ReasonPhrase;
            }
            return View(destinos);
        }

        [HttpGet]
        public ActionResult Create()
        {
            Destination destination = new Destination();
            return View(destination);
        }

        [HttpPost]
        public ActionResult Create(Destination model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.mensajeNegativo = "El modelo no es valido, Revise los datos";
                return View(model);
            }

            if (string.IsNullOrEmpty(model.DestinationCode) || string.IsNullOrEmpty(model.DestinationName))
            {
                ViewBag.mensajeNegativo = "Debe indicar un codigo y un nombre para el nuevo destino..";
                return View(model);
            }

            var resultado = client.PostAsJsonAsync("api/destinations", model).Result;
            if (resultado.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", new { mensajePositivo = "Informacion guardada de forma Exitosa" });
            }
            else
            {
                ViewBag.mensajeNegativo = resultado.ReasonPhrase;
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Update(int id)
        {
            var resultado = client.GetAsync("api/destinations/" + id).Result;
            if (resultado.IsSuccessStatusCode)
            {
                var destino = resultado.Content.ReadAsAsync<Destination>().Result;
                return View(destino);
            }
            else
            {
                return RedirectToAction("Index", new { mensajeNegativo = resultado.ReasonPhrase });
            }
        }

        /// <summary>
        /// Metodo para hacer Update de un destino.
        /// el metodo redirige al index con una mensaje positivo o negativo dependiendo del resultado de la operacion en la api
        /// </summary>
        /// <param name="destination">el destino que se desea modificar</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(Destination destination)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.mensajeNegativo = "El modelo de datos que introdjo no es valido";
                return View(destination);
            }
            var resultado = client.PutAsJsonAsync("api/destinations/" + destination.DestinationID, destination).Result;
            if (resultado.IsSuccessStatusCode)
            {
                //var destino = resultado.Content.ReadAsAsync<Destination>().Result;
                return RedirectToAction("Index", new { mensajePositivo = "Informacion guardada de manera exitosa" });
            }
            else
            {
                return RedirectToAction("Index", new { mensajeNegativo = resultado.ReasonPhrase });
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var resultado = client.GetAsync("api/destinations/" + id).Result;
            if (resultado.IsSuccessStatusCode)
            {
                var destino = resultado.Content.ReadAsAsync<Destination>().Result;
                return View(destino);
            }
            else
            {
                return RedirectToAction("Index", new { mensajeNegativo = resultado.ReasonPhrase });
            }
        }

        [HttpPost]
        public ActionResult Delete(Destination destination)
        {
            var resultado = client.DeleteAsync("api/destinations/" + destination.DestinationID).Result;
            if (resultado.IsSuccessStatusCode)
            {
                //var destino = resultado.Content.ReadAsAsync<Destination>().Result;
                return RedirectToAction("Index", new { mensajePositivo = "Informacion guardada de manera exitosa" });
            }
            else
            {
                return RedirectToAction("Index", new { mensajeNegativo = resultado.ReasonPhrase });
            }
        }
    }
}