﻿using AirWaysClient.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace AirWaysClient.Controllers
{
    public class TravelRegistrationsController : Controller
    {
        /// <summary>
        /// Cliente Http para hacer los request a la Api.
        /// Este objeto lo creo de forma estatica para poder reutilizarlo en toda la aplicacion sin tener que
        /// instanciarlo nuevamente. asi se evitan errores de conexiones a la api.
        /// </summary>
        private static HttpClient client = HttpClientTaker.GetClient();

        /// <summary>
        /// Vuelos Registrados, es una cache temporal para evitar demasiados llamados a la api.
        /// </summary>
        private static List<TravelViewModel> Travels = new List<TravelViewModel>();
        /// <summary>
        /// Clientes registrados, es una cache temporal para evitar demasiados llamados a la api.
        /// </summary>
        private static List<Customer> Customers = new List<Customer>();


        // GET: TravelRegistrations
        public ActionResult Index(string mensajePositivo = null, string mensajeNegativo = null)
        {
            //Setear las propiedades de los mensajes que se van a mostrar en el fromnt.
            if (mensajePositivo != null)
            {
                ViewBag.mensajePositivo = mensajePositivo;
            }
            if (mensajeNegativo != null)
            {
                ViewBag.mensajeNegativo = mensajeNegativo;
            }

            List<TravelRegistrationViewModel> destinos = new List<TravelRegistrationViewModel>();
            var resultado = client.GetAsync("api/TravelRegistrations").Result;
            if (resultado.IsSuccessStatusCode)
            {
                destinos = resultado.Content.ReadAsAsync<List<TravelRegistrationViewModel>>().Result;
            }
            else
            {
                ViewBag.mensajeNegativo = resultado.ReasonPhrase;
            }
            return View(destinos);
        }

        [HttpGet]
        public ActionResult Create()
        {
            //busco la lista de clientes y l almaceno de forma temporal para hacer el viewbag yendo a la api la menor cantidas de veces posible.
            //lo mismo hago con los vuelos disponibles
            Customers = client.GetAsync("api/customers").Result.Content.ReadAsAsync<List<Customer>>().Result;
            ViewBag.customers = new SelectList(Customers, "CustomerID", "CustomerName");

            Travels = client.GetAsync("api/travels").Result.Content.ReadAsAsync<List<TravelViewModel>>().Result;

            ViewBag.travels = new SelectList(Travels, "TravelID", "InfoTravel");
            TravelRegistration registration = new TravelRegistration();
            return View(registration);
        }

        public ActionResult Create(TravelRegistration travel)
        {
            ViewBag.customers = new SelectList(Customers, "CustomerID", "CustomerName");
            ViewBag.travels = new SelectList(Travels, "TravelID", "InfoTravel");
            if (travel.TravelSeatsBooked <= 0)
            {
                ViewBag.mensajeNegativo = "Seleccione la cantidad de asientos que desea reservar";
                return View(travel);
            }
            travel.TravelDate = Travels.Where(t => t.TravelID == travel.TravelID).Select(t => t.TravelDate).FirstOrDefault();
            travel.TRavelPrice = Travels.Where(t => t.TravelID == travel.TravelID).Select(t => t.TravelPrice).FirstOrDefault();
            travel.TravelRegistrationDate = DateTime.Now;
            var resultado = client.PostAsJsonAsync("api/TravelRegistrations", travel).Result;
            if (resultado.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", new { mensajePositivo = "Informacion guardada de forma Exitosa" });
            }
            else
            {
                ViewBag.mensajeNegativo = resultado.ReasonPhrase;
                return View(travel);
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            //busco la lista de clientes y l almaceno de forma temporal para hacer el viewbag yendo a la api la menor cantidas de veces posible.
            //lo mismo hago con los vuelos disponibles
            Customers = client.GetAsync("api/customers").Result.Content.ReadAsAsync<List<Customer>>().Result;
            ViewBag.customers = new SelectList(Customers, "CustomerID", "CustomerName");

            Travels = client.GetAsync("api/travels").Result.Content.ReadAsAsync<List<TravelViewModel>>().Result;
            ViewBag.travels = new SelectList(Travels, "TravelID", "InfoTravel");

            var resultado = client.GetAsync("api/TravelRegistrations/" + id).Result;
            if (resultado.IsSuccessStatusCode)
            {
                var registro = resultado.Content.ReadAsAsync<TravelRegistrationViewModel>().Result;
                //a partir del vewmodel de travelRegistration armo el modelo que necesita la vista.
                TravelRegistration travel = new TravelRegistration
                {
                    CustomerID = registro.CustomerID,
                    TravelID = registro.TravelID,
                    TravelSeatsBooked = registro.TravelSeatsBooked,
                    TravelRegistrationDate = registro.TravelRegistrationDate,
                    TravelRegistrationID = registro.TravelRegistrationID
                };
                return View(travel);
            }
            else
            {
                return RedirectToAction("Index", new { mensajeNegativo = resultado.ReasonPhrase });
            }
        }

        [HttpPost]
        public ActionResult Edit(TravelRegistration travel)
        {
            ViewBag.customers = new SelectList(Customers, "CustomerID", "CustomerName");
            ViewBag.travels = new SelectList(Travels, "TravelID", "InfoTravel");
            if (travel.TravelSeatsBooked <= 0)
            {
                ViewBag.mensajeNegativo = "Seleccione la cantidad de asientos que desea reservar";
                return View(travel);
            }
            travel.TravelDate = Travels.Where(t => t.TravelID == travel.TravelID).Select(t => t.TravelDate).FirstOrDefault();
            travel.TRavelPrice = Travels.Where(t => t.TravelID == travel.TravelID).Select(t => t.TravelPrice).FirstOrDefault();
            var resultado = client.PutAsJsonAsync("api/TravelRegistrations/" + travel.TravelRegistrationID, travel).Result;
            if (resultado.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", new { mensajePositivo = "Informacion guardada de forma Exitosa" });
            }
            else
            {
                ViewBag.mensajeNegativo = resultado.ReasonPhrase;
                return View(travel);
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var resultado = client.GetAsync("api/TravelRegistrations/" + id).Result;
            if (resultado.IsSuccessStatusCode)
            {
                var registro = resultado.Content.ReadAsAsync<TravelRegistrationViewModel>().Result;
                return View(registro);
            }
            else
            {
                return RedirectToAction("Index", new { mensajeNegativo = resultado.ReasonPhrase });
            }
        }

        [HttpPost]
        public ActionResult Delete(TravelRegistrationViewModel travel)
        {
            var resultado = client.DeleteAsync("api/TravelRegistrations/" + travel.TravelRegistrationID).Result;
            if (resultado.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", new { mensajePositivo = "Informacion guardada de manera exitosa" });
            }
            else
            {
                return RedirectToAction("Index", new { mensajeNegativo = resultado.ReasonPhrase });
            }
        }

        /// <summary>
        /// metodo para buscar la cantidad de asientos disponibles una vez que el usuario indicó en el front que vuelo quiere reservar
        /// </summary>
        /// <param name="travelid">el vuelo del que quiero conocer su disponibilidad</param>
        /// <returns></returns>
        public JsonResult GetSeatsRemaining(int travelid)
        {
            int seats = Travels.FirstOrDefault(t => t.TravelID == travelid).TravelAvailableSeats;
            return Json(seats, JsonRequestBehavior.AllowGet);
        }
    }
}