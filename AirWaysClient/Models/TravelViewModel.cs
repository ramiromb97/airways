﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirWaysClient.Models
{
    public class TravelViewModel
    {
        public int TravelID { get; set; }
        public int TravelAvailableSeats { get; set; }
        public int TravelTarget { get; set; }
        public int TravelSource { get; set; }
        public decimal TravelPrice { get; set; }
        public DateTime TravelDate { get; set; }
        public int TravelSeatCapacity { get; set; }
        public string CodeTarget { get; set; }
        public string CodeSource { get; set; }

        public string InfoTravel
        {
            get
            {
                return string.Format("{0} -> {1} : {2}", CodeSource.TrimEnd(), CodeTarget.TrimEnd(), TravelDate);
            }
        }
    }
}