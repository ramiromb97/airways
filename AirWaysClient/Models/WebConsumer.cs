﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;

namespace AirWaysClient.Models
{
    public class WebConsumer
    {
        private static HttpClient client = new HttpClient();
        public static T Get<T>(string url)
        {
            try
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["UrlBase"].ToString());
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync(url).Result;
                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    return default(T);
                }
            }
            catch (Exception e)

            {
                throw new Exception(e.Message, e);
            }

        }

        public static T Post<T>(string url, T Object)
        {
            try
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["UrlBase"].ToString());
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.PostAsJsonAsync(url, Object).Result;
                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    return default(T);
                }
            }
            catch (Exception e)

            {
                throw new Exception(e.Message, e);
            }

        }
    }
}