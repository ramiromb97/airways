﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirWaysClient.Models
{
    public class TravelRegistration
    {
        public int TravelRegistrationID { get; set; }
        public int TravelID { get; set; }
        public int CustomerID { get; set; }
        public decimal TRavelPrice { get; set; }
        public System.DateTime TravelDate { get; set; }
        public System.DateTime TravelRegistrationDate { get; set; }
        public int TravelSeatsBooked { get; set; }

    }
}