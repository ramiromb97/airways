﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirWaysClient.Models
{
    public class Customer
    {
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAdress { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerIDCard { get; set; }
    }
}