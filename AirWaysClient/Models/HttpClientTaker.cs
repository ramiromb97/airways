﻿using System.Web;
using System.Net.Http;
using System.Configuration;
using System;

namespace AirWaysClient.Models
{
    /// <summary>
    /// RM - clase para instanciar el httpcliente que se usar'a en todas las solicitudes del controlador a la api.
    /// dicho cliente ya tiene la urlbase, el token de acceo a la api (si ya se ha hecho loguin en la api) y el tipo de datos que acepta (Json)
    /// </summary>
    public class HttpClientTaker
    {
        /// <summary>
        /// instanciar un httpclient listo para hacer llamados a la api
        /// </summary>
        /// <returns>HttpClient</returns>
        public static HttpClient GetClient()
        {

            HttpClient client = new HttpClient()
            {
                BaseAddress = new Uri(ConfigurationManager.AppSettings["UrlBase"].ToString()),

            };
            //string token = GetTokenFromCookie();
            //if (!string.IsNullOrEmpty(token))
            //{
            //    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            //}
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        public static HttpClient GetTokenFromCookieAndSetToHttpClient(HttpClient c)
        {
            c.DefaultRequestHeaders.Authorization = null;
            string tkn = string.Empty;
            var cookie = HttpContext.Current.Request.Cookies.Get("tkn");
            if (cookie != null)
            {
                c.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", cookie.Value);
            }
            return c;
        }
    }
}