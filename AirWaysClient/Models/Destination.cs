﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirWaysClient.Models
{
    public class Destination
    {
        public int DestinationID { get; set; }
        public string DestinationCode { get; set; }
        public string DestinationName { get; set; }
    }
}