﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirWaysClient.Models
{
    public class TravelRegistrationViewModel
    {
        public int TravelRegistrationID { get; set; }
        public int TravelID { get; set; }
        public int CustomerID { get; set; }
        public decimal TRavelPrice { get; set; }
        public System.DateTime TravelDate { get; set; }
        public System.DateTime TravelRegistrationDate { get; set; }
        public int TravelSeatsBooked { get; set; }
        public int TravelID1 { get; set; }
        public int TravelAvailableSeats { get; set; }
        public int TravelTarget { get; set; }
        public int TravelSource { get; set; }
        public decimal TravelPrice1 { get; set; }
        public DateTime TravelDate1 { get; set; }
        public int TravelSeatCapacity { get; set; }
        public int CustomerID1 { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAdress { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerIDCard { get; set; }
        public string Destino { get; set; }
        public string Origen { get; set; }
    }
}