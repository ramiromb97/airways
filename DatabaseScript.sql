USE [master]
GO
/****** Object:  Database [AirWaysDB]    Script Date: 06/15/2019 5:32:48 PM ******/
CREATE DATABASE [AirWaysDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'AirWaysDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\AirWaysDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'AirWaysDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\AirWaysDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [AirWaysDB] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AirWaysDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [AirWaysDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [AirWaysDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [AirWaysDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [AirWaysDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [AirWaysDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [AirWaysDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [AirWaysDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [AirWaysDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [AirWaysDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [AirWaysDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [AirWaysDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [AirWaysDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [AirWaysDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [AirWaysDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [AirWaysDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [AirWaysDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [AirWaysDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [AirWaysDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [AirWaysDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [AirWaysDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [AirWaysDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [AirWaysDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [AirWaysDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [AirWaysDB] SET  MULTI_USER 
GO
ALTER DATABASE [AirWaysDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [AirWaysDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [AirWaysDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [AirWaysDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [AirWaysDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [AirWaysDB] SET QUERY_STORE = OFF
GO
USE [AirWaysDB]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [AirWaysDB]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 06/15/2019 5:32:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [nvarchar](max) NOT NULL,
	[CustomerAdress] [nvarchar](max) NOT NULL,
	[CustomerPhone] [nvarchar](50) NOT NULL,
	[CustomerIDCard] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Destinations]    Script Date: 06/15/2019 5:32:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Destinations](
	[DestinationID] [int] IDENTITY(1,1) NOT NULL,
	[DestinationCode] [nchar](10) NULL,
	[DestinationName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Destinations] PRIMARY KEY CLUSTERED 
(
	[DestinationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TravelRegistration]    Script Date: 06/15/2019 5:32:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TravelRegistration](
	[TravelRegistrationID] [int] IDENTITY(1,1) NOT NULL,
	[TravelID] [int] NOT NULL,
	[CustomerID] [int] NOT NULL,
	[TRavelPrice] [decimal](18, 2) NOT NULL,
	[TravelDate] [datetime] NOT NULL,
	[TravelRegistrationDate] [datetime] NOT NULL,
	[TravelSeatsBooked] [int] NOT NULL,
 CONSTRAINT [PK_TravelRegistration] PRIMARY KEY CLUSTERED 
(
	[TravelRegistrationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Travels]    Script Date: 06/15/2019 5:32:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Travels](
	[TravelID] [int] IDENTITY(1,1) NOT NULL,
	[TravelAvailableSeats] [int] NULL,
	[TravelTarget] [int] NOT NULL,
	[TravelSource] [int] NOT NULL,
	[TravelPrice] [decimal](18, 2) NULL,
	[TravelDate] [datetime] NULL,
	[TravelSeatCapacity] [int] NULL,
 CONSTRAINT [PK_Travels] PRIMARY KEY CLUSTERED 
(
	[TravelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([CustomerID], [CustomerName], [CustomerAdress], [CustomerPhone], [CustomerIDCard]) VALUES (1, N'Ramiro Marimon', N'Guarenas Km 15', N'0412-6046582e', N'V-19561143')
INSERT [dbo].[Customers] ([CustomerID], [CustomerName], [CustomerAdress], [CustomerPhone], [CustomerIDCard]) VALUES (3, N'Deymer Pacheco', N'petare petare', N'0412-6047770', N'V-24906103')
SET IDENTITY_INSERT [dbo].[Customers] OFF
SET IDENTITY_INSERT [dbo].[Destinations] ON 

INSERT [dbo].[Destinations] ([DestinationID], [DestinationCode], [DestinationName]) VALUES (2, N'COL       ', N'ColombiaCampeon')
INSERT [dbo].[Destinations] ([DestinationID], [DestinationCode], [DestinationName]) VALUES (9, N'Bra       ', N'Brasil')
INSERT [dbo].[Destinations] ([DestinationID], [DestinationCode], [DestinationName]) VALUES (10, N'USA       ', N'Estados Unidos')
INSERT [dbo].[Destinations] ([DestinationID], [DestinationCode], [DestinationName]) VALUES (11, N'RUS       ', N'Rusia')
SET IDENTITY_INSERT [dbo].[Destinations] OFF
SET IDENTITY_INSERT [dbo].[TravelRegistration] ON 

INSERT [dbo].[TravelRegistration] ([TravelRegistrationID], [TravelID], [CustomerID], [TRavelPrice], [TravelDate], [TravelRegistrationDate], [TravelSeatsBooked]) VALUES (1, 2, 1, CAST(1234.56 AS Decimal(18, 2)), CAST(N'2019-06-14T23:32:00.000' AS DateTime), CAST(N'2019-06-15T12:47:40.357' AS DateTime), 8)
INSERT [dbo].[TravelRegistration] ([TravelRegistrationID], [TravelID], [CustomerID], [TRavelPrice], [TravelDate], [TravelRegistrationDate], [TravelSeatsBooked]) VALUES (3, 3, 1, CAST(200.00 AS Decimal(18, 2)), CAST(N'2019-07-12T09:10:00.000' AS DateTime), CAST(N'2019-06-15T13:09:52.527' AS DateTime), 5)
INSERT [dbo].[TravelRegistration] ([TravelRegistrationID], [TravelID], [CustomerID], [TRavelPrice], [TravelDate], [TravelRegistrationDate], [TravelSeatsBooked]) VALUES (4, 4, 3, CAST(2222.00 AS Decimal(18, 2)), CAST(N'2019-06-15T23:30:00.000' AS DateTime), CAST(N'2019-06-15T13:36:51.340' AS DateTime), 6)
INSERT [dbo].[TravelRegistration] ([TravelRegistrationID], [TravelID], [CustomerID], [TRavelPrice], [TravelDate], [TravelRegistrationDate], [TravelSeatsBooked]) VALUES (5, 5, 3, CAST(233.00 AS Decimal(18, 2)), CAST(N'2019-06-15T07:42:00.000' AS DateTime), CAST(N'2019-06-15T13:50:22.183' AS DateTime), 7)
SET IDENTITY_INSERT [dbo].[TravelRegistration] OFF
SET IDENTITY_INSERT [dbo].[Travels] ON 

INSERT [dbo].[Travels] ([TravelID], [TravelAvailableSeats], [TravelTarget], [TravelSource], [TravelPrice], [TravelDate], [TravelSeatCapacity]) VALUES (2, 84, 11, 10, CAST(1234.56 AS Decimal(18, 2)), CAST(N'2019-06-14T23:32:00.000' AS DateTime), 92)
INSERT [dbo].[Travels] ([TravelID], [TravelAvailableSeats], [TravelTarget], [TravelSource], [TravelPrice], [TravelDate], [TravelSeatCapacity]) VALUES (3, 24, 2, 9, CAST(200.00 AS Decimal(18, 2)), CAST(N'2019-07-12T09:10:00.000' AS DateTime), 24)
INSERT [dbo].[Travels] ([TravelID], [TravelAvailableSeats], [TravelTarget], [TravelSource], [TravelPrice], [TravelDate], [TravelSeatCapacity]) VALUES (4, 0, 2, 10, CAST(2222.00 AS Decimal(18, 2)), CAST(N'2019-06-15T23:30:00.000' AS DateTime), 11)
INSERT [dbo].[Travels] ([TravelID], [TravelAvailableSeats], [TravelTarget], [TravelSource], [TravelPrice], [TravelDate], [TravelSeatCapacity]) VALUES (5, 293, 10, 11, CAST(233.00 AS Decimal(18, 2)), CAST(N'2019-06-15T07:42:00.000' AS DateTime), 300)
SET IDENTITY_INSERT [dbo].[Travels] OFF
ALTER TABLE [dbo].[TravelRegistration]  WITH CHECK ADD  CONSTRAINT [FK_TravelRegistration_Customers] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[TravelRegistration] CHECK CONSTRAINT [FK_TravelRegistration_Customers]
GO
ALTER TABLE [dbo].[TravelRegistration]  WITH CHECK ADD  CONSTRAINT [FK_TravelRegistration_Travels] FOREIGN KEY([TravelID])
REFERENCES [dbo].[Travels] ([TravelID])
GO
ALTER TABLE [dbo].[TravelRegistration] CHECK CONSTRAINT [FK_TravelRegistration_Travels]
GO
ALTER TABLE [dbo].[Travels]  WITH CHECK ADD  CONSTRAINT [FK_Travels_Destinations] FOREIGN KEY([TravelTarget])
REFERENCES [dbo].[Destinations] ([DestinationID])
GO
ALTER TABLE [dbo].[Travels] CHECK CONSTRAINT [FK_Travels_Destinations]
GO
ALTER TABLE [dbo].[Travels]  WITH CHECK ADD  CONSTRAINT [FK_Travels_Destinations1] FOREIGN KEY([TravelSource])
REFERENCES [dbo].[Destinations] ([DestinationID])
GO
ALTER TABLE [dbo].[Travels] CHECK CONSTRAINT [FK_Travels_Destinations1]
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetSeatsBookedForTravel]    Script Date: 06/15/2019 5:32:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_GetSeatsBookedForTravel] 
	-- Add the parameters for the stored procedure here
	@travelID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ISNULL(sum(t.TravelSeatsBooked), 0) as SeatsBooked from TravelRegistration t where t.TravelID = @travelID
END

GO
/****** Object:  StoredProcedure [dbo].[Sp_GetTravel]    Script Date: 06/15/2019 5:32:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_GetTravel] 
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *, 
	(select d.DestinationCode from Destinations d where d.DestinationID = t.TravelTarget) as CodeTarget, 
	(select d.DestinationCode from Destinations d where d.DestinationID = t.TravelSource) as CodeSource
	from Travels t where t.TravelID = @id
END

GO
/****** Object:  StoredProcedure [dbo].[Sp_GetTravelRegistration]    Script Date: 06/15/2019 5:32:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_GetTravelRegistration] 
	-- Add the parameters for the stored procedure here
	@TravelRegistrationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT tr.CustomerID, tr.TravelDate, tr.TravelID, tr.TRavelPrice, tr.TravelRegistrationDate, tr.TravelRegistrationID, tr.TravelSeatsBooked,
	t.TravelAvailableSeats, t.TravelSeatCapacity, t.TravelDate as TravelDate1, t.TravelPrice as TravelPrice1, t.TravelSource, t.TravelID as TravelID1, TravelTarget,
	c.CustomerAdress, c.CustomerID as CustomerID1, c.CustomerIDCard, c.CustomerName, c.CustomerPhone, (select d.DestinationCode from Destinations d where d.DestinationID = t.TravelTarget ) as Destino,
	(select d.DestinationCode from Destinations d where d.DestinationID = t.TravelSource) as Origen
	from TravelRegistration tr
	inner join Travels t on t.TravelID = tr.TravelID
	inner join Customers c on c.CustomerID = tr.CustomerID
	where tr.TravelRegistrationID = @TravelRegistrationID
END

GO
/****** Object:  StoredProcedure [dbo].[Sp_GetTravelRegistrations]    Script Date: 06/15/2019 5:32:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_GetTravelRegistrations]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tr.CustomerID, tr.TravelDate, tr.TravelID, tr.TRavelPrice, tr.TravelRegistrationDate, tr.TravelRegistrationID, tr.TravelSeatsBooked,
	t.TravelAvailableSeats, t.TravelSeatCapacity, t.TravelDate as TravelDate1, t.TravelPrice as TravelPrice1, t.TravelSource, t.TravelID as TravelID1, TravelTarget,
	c.CustomerAdress, c.CustomerID as CustomerID1, c.CustomerIDCard, c.CustomerName, c.CustomerPhone, 
	(select d.DestinationCode from Destinations d where d.DestinationID = t.TravelTarget ) as Destino,
	(select d.DestinationCode from Destinations d where d.DestinationID = t.TravelSource) as Origen
	from TravelRegistration tr
	inner join Travels t on t.TravelID = tr.TravelID
	inner join Customers c on c.CustomerID = tr.CustomerID
END

GO
/****** Object:  StoredProcedure [dbo].[Sp_GetTravels]    Script Date: 06/15/2019 5:32:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_GetTravels]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *, 
	(select d.DestinationCode from Destinations d where d.DestinationID = t.TravelTarget) as CodeTarget, 
	(select d.DestinationCode from Destinations d where d.DestinationID = t.TravelSource) as CodeSource
	from Travels t
END

GO
USE [master]
GO
ALTER DATABASE [AirWaysDB] SET  READ_WRITE 
GO
