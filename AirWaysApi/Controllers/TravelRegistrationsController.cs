﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AirWaysApi;

namespace AirWaysApi.Controllers
{
    [Authorize]
    public class TravelRegistrationsController : ApiController
    {
        private AirWaysDBEntities db = new AirWaysDBEntities();

        // GET: api/TravelRegistrations
        public List<Sp_GetTravelRegistrations_Result> GetTravelRegistrations()
        {
            var registrations = db.Sp_GetTravelRegistrations().ToList();
            return registrations;
        }

        // GET: api/TravelRegistrations/5
        [ResponseType(typeof(Sp_GetTravelRegistrations_Result))]
        public IHttpActionResult GetTravelRegistration(int id)
        {
            var registration = db.Sp_GetTravelRegistration(id).FirstOrDefault();
            if (registration == null)
            {
                return NotFound();
            }

            return Ok(registration);
        }

        // PUT: api/TravelRegistrations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTravelRegistration(int id, TravelRegistration travelRegistration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != travelRegistration.TravelRegistrationID)
            {
                return BadRequest();
            }

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    TravelRegistration OriginalTravelReg = db.TravelRegistrations.FirstOrDefault(t => t.TravelRegistrationID == id); //el travelregistration original
                    Travel travelOriginal = db.Travels.FirstOrDefault(t => t.TravelID == OriginalTravelReg.TravelID); //Travel asociado al travelRegistration
                    int CantidadAsintosOriginales = OriginalTravelReg.TravelSeatsBooked; //cantidad de asientos originalmente reservada
                                                                                         //setear las nuevas propiedades del objeto
                    OriginalTravelReg.CustomerID = travelRegistration.CustomerID;
                    OriginalTravelReg.TravelSeatsBooked = travelRegistration.TravelSeatsBooked;
                    if (travelOriginal.TravelID == travelRegistration.TravelID)
                    {
                        OriginalTravelReg.TRavelPrice = travelOriginal.TravelPrice.Value;
                        OriginalTravelReg.TravelDate = travelOriginal.TravelDate.Value;
                        travelOriginal.TravelAvailableSeats += (+CantidadAsintosOriginales - travelRegistration.TravelSeatsBooked); //actualizar la cantidad de asientos disponibles
                        db.Entry(travelOriginal).State = EntityState.Modified;
                        db.Entry(OriginalTravelReg).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                        return StatusCode(HttpStatusCode.NoContent);

                    }
                    else
                    {
                        travelOriginal.TravelAvailableSeats += CantidadAsintosOriginales;
                        Travel newTravel = db.Travels.FirstOrDefault(t => t.TravelID == travelRegistration.TravelID);
                        newTravel.TravelAvailableSeats -= travelRegistration.TravelSeatsBooked;
                        OriginalTravelReg.TravelDate = newTravel.TravelDate.Value;
                        db.Entry(travelOriginal).State = EntityState.Modified;
                        db.Entry(newTravel).State = EntityState.Modified;
                        db.Entry(OriginalTravelReg).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                        return StatusCode(HttpStatusCode.NoContent);

                    }
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return InternalServerError(e);
                }


            }

        }

        // POST: api/TravelRegistrations
        [ResponseType(typeof(TravelRegistration))]
        public IHttpActionResult PostTravelRegistration(TravelRegistration travelRegistration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //manejo transaccinal del guardado para evitar errores ya que se involucran varias tablas
            using (var transaccion = db.Database.BeginTransaction())
            {
                try
                {
                    var travel = db.Travels.FirstOrDefault(t => t.TravelID == travelRegistration.TravelID);
                    travel.TravelAvailableSeats -= travelRegistration.TravelSeatsBooked;
                    db.Entry(travel).State = EntityState.Modified;
                    db.TravelRegistrations.Add(travelRegistration);
                    db.SaveChanges();
                    transaccion.Commit();
                    return CreatedAtRoute("DefaultApi", new { id = travelRegistration.TravelRegistrationID }, travelRegistration);
                }
                catch (Exception e)
                {
                    transaccion.Rollback();
                    return InternalServerError(e);
                }
            }

        }

        // DELETE: api/TravelRegistrations/5
        [ResponseType(typeof(TravelRegistration))]
        public IHttpActionResult DeleteTravelRegistration(int id)
        {
            TravelRegistration travelRegistration = db.TravelRegistrations.Find(id);
            if (travelRegistration == null)
            {
                return NotFound();
            }
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var originalTavel = db.Travels.FirstOrDefault(t => t.TravelID == travelRegistration.TravelID);
                    originalTavel.TravelAvailableSeats += travelRegistration.TravelSeatsBooked;
                    db.Entry(originalTavel).State = EntityState.Modified;
                    db.TravelRegistrations.Remove(travelRegistration);
                    db.SaveChanges();
                    transaction.Commit();
                    return Ok();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return InternalServerError(e);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TravelRegistrationExists(int id)
        {
            return db.TravelRegistrations.Count(e => e.TravelRegistrationID == id) > 0;
        }
    }
}