//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AirWaysApi
{
    using System;
    
    public partial class Sp_GetTravels_Result
    {
        public int TravelID { get; set; }
        public Nullable<int> TravelAvailableSeats { get; set; }
        public int TravelTarget { get; set; }
        public int TravelSource { get; set; }
        public Nullable<decimal> TravelPrice { get; set; }
        public Nullable<System.DateTime> TravelDate { get; set; }
        public Nullable<int> TravelSeatCapacity { get; set; }
        public string CodeTarget { get; set; }
        public string CodeSource { get; set; }
    }
}
