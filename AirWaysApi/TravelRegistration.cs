//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AirWaysApi
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    
    public partial class TravelRegistration
    {
        public int TravelRegistrationID { get; set; }
        public int TravelID { get; set; }
        public int CustomerID { get; set; }
        public decimal TRavelPrice { get; set; }
        public System.DateTime TravelDate { get; set; }
        public System.DateTime TravelRegistrationDate { get; set; }
        public int TravelSeatsBooked { get; set; }
    
        [JsonIgnore]
        public virtual Customer Customer { get; set; }
        [JsonIgnore]
        public virtual Travel Travel { get; set; }
    }
}
